﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LibreriaExperto
{
    public static class ApiConfiguracion
    {
        public static HttpClient Inicializar()
        {
            var cliente = new HttpClient();
            //http://jsebastianmartin-001-site1.itempurl.com/
            //Notebook casa Seba https://localhost:44312/
            //PC Casa Seba https://localhost:44371/
            
            cliente.BaseAddress = new Uri("https://localhost:44371/");
            return cliente;
        }
    }
}
