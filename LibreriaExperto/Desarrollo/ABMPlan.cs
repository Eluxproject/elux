﻿using LibreriaClases;
using LibreriaClases.DTO;
using LibreriaClases.Transferencia;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LibreriaExperto.Desarrollo
{
    public static class ABMPlan
    {
        public static ErrorPropy CrearPlan(DTOPlan dTOPlan)
        {
            ErrorPropy errorPropy = new ErrorPropy();
            HttpClient httpClient = ApiConfiguracion.Inicializar();
            TransferenciaPlan transferenciaPlan = new TransferenciaPlan();

            transferenciaPlan.planId = System.Guid.NewGuid().ToString();
            transferenciaPlan.nombrePlan = dTOPlan.nombrePlan;
            transferenciaPlan.precioPlan = dTOPlan.precioPlan;
            transferenciaPlan.TipoMonedaID = dTOPlan.tipoMonedaId;
            transferenciaPlan.cantidadCreditosIniciales = dTOPlan.cantidadCreditosIniciales;
            transferenciaPlan.cantidadMaxImagenesPermitidasPorPublicacion = dTOPlan.cantidadMaxImagenesPermitidasPorPublicacion;
            transferenciaPlan.permiteVideo = dTOPlan.permiteVideo;
            transferenciaPlan.accesoEstadisticasAvanzadas = dTOPlan.accesoEstadisticasAvanzadas;
            transferenciaPlan.activo = true;
           
            var tareaCrearPLan = httpClient.PostAsJsonAsync<TransferenciaPlan>("api/Plan/crearPlan",transferenciaPlan);
            tareaCrearPLan.Wait();

            if (!tareaCrearPLan.Result.IsSuccessStatusCode)
            {
                errorPropy.codigoError = (int)tareaCrearPLan.Result.StatusCode;
                errorPropy.descripcionError = "Error: " + errorPropy.codigoError + " " + tareaCrearPLan.Result.StatusCode;
            }

            return errorPropy;
        }
    }
}
