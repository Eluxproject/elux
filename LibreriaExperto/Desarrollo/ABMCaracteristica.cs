﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using LibreriaClases;
using LibreriaClases.DTO;
using LibreriaClases.Transferencia;

namespace LibreriaExperto.Desarrollo
{
    public static class ABMCaracteristica
    {
        public static (ErrorPropy, List<DTOCaracteristica>) TraerCaracteristicas()
        {
            List<DTOCaracteristica> ListaCaracteristica = new List<DTOCaracteristica>();
            ErrorPropy errorPropy = new ErrorPropy();
            HttpClient httpClient = ApiConfiguracion.Inicializar();

            var tareaTraerCaracteristica = httpClient.GetAsync("api/Caracteristica/obtenerCaracteristicas");
            tareaTraerCaracteristica.Wait();

            if (!tareaTraerCaracteristica.Result.IsSuccessStatusCode)
            {
                errorPropy.codigoError = (int)tareaTraerCaracteristica.Result.StatusCode;
                errorPropy.descripcionError = "Error: " + errorPropy.codigoError + " " + tareaTraerCaracteristica.Result.StatusCode;
                ListaCaracteristica = null;
            }
            else
            {
                List<TransferenciaCaracteristica> transferenciaCaracteristica = tareaTraerCaracteristica.Result.Content.ReadAsAsync<List<TransferenciaCaracteristica>>().Result;

                foreach(var TC in transferenciaCaracteristica)
                {
                    DTOCaracteristica dTOCaracteristica = new DTOCaracteristica();
                    dTOCaracteristica.caracteristicaId = TC.caracteristicaId;
                    dTOCaracteristica.nombreCaracteristica = TC.nombreCaracteristica;

                    ListaCaracteristica.Add(dTOCaracteristica);
                }
            }


            return (errorPropy, ListaCaracteristica);
        }
    }
}
