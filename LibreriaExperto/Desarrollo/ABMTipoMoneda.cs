﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using LibreriaClases;
using LibreriaClases.DTO;
using LibreriaClases.Transferencia;

namespace LibreriaExperto.Desarrollo
{
    public static class ABMTipoMoneda
    {
        public static (ErrorPropy, List<DTOTipoMoneda>) traerTipoMoneda()
        {
            ErrorPropy errorPropy = new ErrorPropy();
            List<DTOTipoMoneda> ListaMonedas = new List<DTOTipoMoneda>();
            HttpClient httpClient = ApiConfiguracion.Inicializar();

            var tareaTraerMonedas = httpClient.GetAsync("api/TipoMoneda/obtenerTiposMonedas");
            tareaTraerMonedas.Wait();

            if (!tareaTraerMonedas.Result.IsSuccessStatusCode)
            {
                errorPropy.codigoError = (int)tareaTraerMonedas.Result.StatusCode;
                errorPropy.descripcionError = "Error: " + errorPropy.codigoError + " " + tareaTraerMonedas.Result.StatusCode;
                ListaMonedas = null;
            }
            else
            {
                List<TransferenciaTipoMoneda> transferenciaTipoMonedas = tareaTraerMonedas.Result.Content.ReadAsAsync<List<TransferenciaTipoMoneda>>().Result;
                foreach (var TM in transferenciaTipoMonedas)
                {
                    DTOTipoMoneda dTOTipoMoneda = new DTOTipoMoneda();
                    dTOTipoMoneda.denominacionMoneda = TM.denominacionMoneda;
                    dTOTipoMoneda.tipoMonedaId = TM.tipoMonedaId;

                    ListaMonedas.Add(dTOTipoMoneda);
                }
            }

            return (errorPropy, ListaMonedas);
        }
    }
}
