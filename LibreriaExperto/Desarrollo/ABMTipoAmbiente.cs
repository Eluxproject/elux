﻿using LibreriaClases.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using LibreriaClases;
using System.Net.Http;
using LibreriaClases.Transferencia;

namespace LibreriaExperto.Desarrollo
{
    static public class ABMTipoAmbiente
    {
        public static (ErrorPropy,List<DTOTipoAmbiente>) TraerTipoAmbiente()
        {
            ErrorPropy errorPropy = new ErrorPropy();
            List<DTOTipoAmbiente> ListaDTOTIposAmbiente = new List<DTOTipoAmbiente>();
            HttpClient clienteHttp = ApiConfiguracion.Inicializar();
           
            var tareaObtenerTipoAmbiente = clienteHttp.GetAsync("api/TipoAmbiente/obtenerTiposAmbientes");
            tareaObtenerTipoAmbiente.Wait();

            if (!tareaObtenerTipoAmbiente.Result.IsSuccessStatusCode)
            {
                errorPropy.codigoError = (int)tareaObtenerTipoAmbiente.Result.StatusCode;
                errorPropy.descripcionError = "Error: " + errorPropy.codigoError + " " + tareaObtenerTipoAmbiente.Result.StatusCode;
                ListaDTOTIposAmbiente = null;
            }
            else
            {
                List<TransferenciaTipoAmbiente> tiposAmbientes = tareaObtenerTipoAmbiente.Result.Content.ReadAsAsync<List<TransferenciaTipoAmbiente>>().Result;
               
                foreach (var tipoAmb in tiposAmbientes)
                {
                    DTOTipoAmbiente dTOTipoAmbiente = new DTOTipoAmbiente();
                    dTOTipoAmbiente.nombreTipoAmbiente = tipoAmb.nombreTipoAmbiente;
                    dTOTipoAmbiente.tipoAmbienteId = tipoAmb.tipoAmbienteId;

                    ListaDTOTIposAmbiente.Add(dTOTipoAmbiente);
                }
            }

            return (errorPropy, ListaDTOTIposAmbiente);
        }
    }
}
