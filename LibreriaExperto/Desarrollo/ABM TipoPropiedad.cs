﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using LibreriaClases;
using LibreriaClases.DTO;
using LibreriaClases.Transferencia;

namespace LibreriaExperto.Desarrollo
{
    public static class ABM_TipoPropiedad
    {
        public static ErrorPropy CrearTipoPropiedad(string nombreTipoPropiedad,List<string> tiposAmbientesID) {
            ErrorPropy error = new ErrorPropy();
            HttpClient clienteHttp = ApiConfiguracion.Inicializar();

            #region Crear instancia de Tipo de Propiedad
            TransferenciaTipoPropiedad tipoPropiedad = new TransferenciaTipoPropiedad();
            tipoPropiedad.tipoPropiedadId = Guid.NewGuid().ToString();
            tipoPropiedad.activo = true;
            tipoPropiedad.nombreTipoPropiedad = nombreTipoPropiedad;
            #endregion
            #region Crear instancia de TipoPropiedadTipoAmbiente
            foreach (var tipoAmbienteID in tiposAmbientesID)
            {
                TransferenciaTipoPropiedadTipoAmbiente tipoPropiedadTipoAmbiente = new TransferenciaTipoPropiedadTipoAmbiente();
                tipoPropiedadTipoAmbiente.tipoPropiedadTipoAmbienteId = Guid.NewGuid().ToString();
                tipoPropiedadTipoAmbiente.activo = true;
                tipoPropiedadTipoAmbiente.tipoPropiedadId = tipoPropiedad.tipoPropiedadId;
                tipoPropiedadTipoAmbiente.tipoAmbienteId = tipoAmbienteID;
                tipoPropiedad.TipoPropiedadTipoAmbiente.Add(tipoPropiedadTipoAmbiente);
            }
            #endregion
            var tareaCrearTipoPropiedad = clienteHttp.PostAsJsonAsync<TransferenciaTipoPropiedad>("api/TipoPropiedad/crearTipoPropiedad",tipoPropiedad);
            tareaCrearTipoPropiedad.Wait();
            if (!tareaCrearTipoPropiedad.Result.IsSuccessStatusCode) {
                error.codigoError = (int)tareaCrearTipoPropiedad.Result.StatusCode;
                error.descripcionError = "Error: " + error.codigoError + " " + tareaCrearTipoPropiedad.Result.StatusCode;
                return error;
            }
          
            return error;
            
        }

        public static (ErrorPropy, List<DTOTipoPropiedad>) TraerTipoPropiedad()
        {
            ErrorPropy errorPropy = new ErrorPropy();
            List<DTOTipoPropiedad> ListaDTOTIpoPropiedad = new List<DTOTipoPropiedad>();
            HttpClient clienteHttp = ApiConfiguracion.Inicializar();

            var tareaObtenerTipoPropiedad = clienteHttp.GetAsync("api/TipoPropiedad/obtenerTiposPropiedades");
            tareaObtenerTipoPropiedad.Wait();

            if (!tareaObtenerTipoPropiedad.Result.IsSuccessStatusCode)
            {
                errorPropy.codigoError = (int)tareaObtenerTipoPropiedad.Result.StatusCode;
                errorPropy.descripcionError = "Error: " + errorPropy.codigoError + " " + tareaObtenerTipoPropiedad.Result.StatusCode;
                ListaDTOTIpoPropiedad = null;
            }
            else
            {
                List<TransferenciaTipoPropiedad> tipoPropiedades= tareaObtenerTipoPropiedad.Result.Content.ReadAsAsync<List<TransferenciaTipoPropiedad>>().Result;

                foreach(var tipoProp in tipoPropiedades)
                {
                    DTOTipoPropiedad dTOTipoPropiedad = new DTOTipoPropiedad();
                    dTOTipoPropiedad.nombreTipoPropiedad = tipoProp.nombreTipoPropiedad;
                    dTOTipoPropiedad.tipoPropiedadId = tipoProp.tipoPropiedadId;

                    ListaDTOTIpoPropiedad.Add(dTOTipoPropiedad);
                }
            }
            return (errorPropy, ListaDTOTIpoPropiedad);
        }
    }
}
