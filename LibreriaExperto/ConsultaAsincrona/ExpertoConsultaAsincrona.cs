﻿using LibreriaClases.Transferencia;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace LibreriaExperto.ConsultaAsincrona
{
    public class ExpertoConsultaAsincrona
    {
        private int CreditosActuales { get; set; }
        public int Consulta_Creditos_Async(string User)
        {
            CreditosActuales = 0;

            HttpClient httpClient = ApiConfiguracion.Inicializar();
            var tareaConsultarCreditos = httpClient.GetAsync("api/PlanUsuario/ObtenerPorID/" + User);
            tareaConsultarCreditos.Wait();

            if (!tareaConsultarCreditos.Result.IsSuccessStatusCode)
            {
                throw new Exception(tareaConsultarCreditos.Result.StatusCode.ToString());
            }
            else
            {
                TransferenciaPlanUsuario transferenciaPlanUsuario = tareaConsultarCreditos.Result.Content.ReadAsAsync<TransferenciaPlanUsuario>().Result;

                CreditosActuales = transferenciaPlanUsuario.cantidadCreditosActivos;
            }
            return CreditosActuales;
        }
    }
}
