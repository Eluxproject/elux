﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaClases.Transferencia
{
    public class TransferenciaPropiedadCaracteristica
    {
        public string propiedadCaracteristicaId { get; set; }
        public bool activo { get; set; }
        public string propiedadId { get; set; }
        public string caracteristicaId { get; set; }
        public virtual TransferenciaCaracteristica Caracteristica { get; set; }
    }
}
