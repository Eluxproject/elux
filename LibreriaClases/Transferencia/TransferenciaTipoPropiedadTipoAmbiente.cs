﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaClases.Transferencia
{
    public class TransferenciaTipoPropiedadTipoAmbiente
    {
        public string tipoPropiedadTipoAmbienteId { get; set; }
        public bool activo { get; set; }
        public string tipoAmbienteId { get; set; }
        public string tipoPropiedadId { get; set; }
        public virtual TransferenciaTipoAmbiente TipoAmbiente { get; set; }
    }
}
