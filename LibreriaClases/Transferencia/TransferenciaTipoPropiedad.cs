﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaClases.Transferencia
{
    public class TransferenciaTipoPropiedad
    {
        public string tipoPropiedadId { get; set; }
        public string nombreTipoPropiedad { get; set; }
        public bool activo { get; set; }

        public virtual ICollection<TransferenciaTipoPropiedadTipoAmbiente> TipoPropiedadTipoAmbiente { get; set; }
    }
}
