﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaClases.Transferencia
{
    public class TransferenciaTipoAmbiente
    {
        public string tipoAmbienteId { get; set; }
        public string nombreTipoAmbiente { get; set; }
        public bool activo { get; set; }
    }
}
