﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaClases.DTO
{
    public class DTOCaracteristica
    {
        public string caracteristicaId { get; set; }
        public string nombreCaracteristica { get; set; }
    }
}
