﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibreriaClases.DTO
{
    public class DTOTipoPropiedad
    {
        public string tipoPropiedadId { get; set; }
        public string nombreTipoPropiedad { get; set; }
    }
}
