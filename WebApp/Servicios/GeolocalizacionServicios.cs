﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;
using WebApp.DTO;

namespace WebApp.Servicios
{
    public class GeolocalizacionServicios
    {
        public DTOUbicacionIP GeolocalizacionIP(string ipss) {
            IPHostEntry IPHost = Dns.GetHostEntry(Dns.GetHostName());
            string ip = new WebClient().DownloadString("https://api.ipify.org");

            string host = Dns.GetHostName();
            IPAddress[] ips = Dns.GetHostAddresses(host);

            const string apiKey = "1be9eb348a53c301e337237774666d754d7711f110e3e890012f5b68385f1b7d";
            string strQuery = "http://api.ipinfodb.com/v3/ip-city/?" + "ip=" + ip + "&key=" + apiKey + "&format=xml";
            //var url = "http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=xml";
            var doc = XDocument.Load(string.Format(strQuery, apiKey, ip));

            DTOUbicacionIP ubicacion = new DTOUbicacionIP();
            ubicacion.pais = doc.Descendants("countryName").First().Value;

            return ubicacion;
        }
    }
}