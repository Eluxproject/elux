﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
using WebApp.Servicios;

namespace WebApp.Controllers
{
    public class TipoAmbienteController : Controller
    {
        private TipoAmbienteServicios servicios;
        public TipoAmbienteController() {
            servicios = new TipoAmbienteServicios();
        }
        // GET: TipoAmbiente
        public ActionResult ListarTiposAmbiente()
        {
            var data = servicios.ListarTipoAmbientes();
            return PartialView(data);
                
        }
    }
}