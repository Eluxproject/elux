﻿using LibreriaClases;
using LibreriaClases.DTO;
using LibreriaExperto.Desarrollo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class ABMPlanController : Controller
    {
        // GET: ABMPlan
        [HttpGet]
        public ActionResult CrearPlan()
        {
            (ErrorPropy errorPropy, List<DTOTipoMoneda> dTOTipoMonedas) resultado = ABMTipoMoneda.traerTipoMoneda();
            try
            {
                if (resultado.errorPropy.codigoError != 0) throw new Exception(resultado.errorPropy.descripcionError);
            }
            catch (Exception error)
            {
                ViewBag.Error = error.Message;
                return View("Error");
            }

            return View(resultado.dTOTipoMonedas);
        }

        [HttpPost]
        public JsonResult CrearPlan(string NombrePlan, string TipoMoneda, bool PermitirVideo, bool PermitirEstadisticas, int Precio = 0, int CantCreditos = 0, int CantImage = 0)
        {
            DTOPlan dTOPlan = new DTOPlan();

            dTOPlan.nombrePlan = NombrePlan;
            dTOPlan.precioPlan = Precio;
            dTOPlan.tipoMonedaId = TipoMoneda;
            dTOPlan.cantidadCreditosIniciales = CantCreditos;
            dTOPlan.cantidadMaxImagenesPermitidasPorPublicacion = CantImage;
            dTOPlan.permiteVideo = PermitirVideo;
            dTOPlan.accesoEstadisticasAvanzadas = PermitirEstadisticas;

            ErrorPropy errorPropy = ABMPlan.CrearPlan(dTOPlan);

            string result = "";

            try
            {
                if (errorPropy.codigoError != 0)
                {
                    result = "Error";
                    throw new Exception(errorPropy.descripcionError);
                }
            }
            catch (Exception error)
            {
                ViewBag.Error = error.Message;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result = "OK";

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}