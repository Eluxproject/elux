﻿using LibreriaClases;
using LibreriaClases.DTO;
using LibreriaExperto.Desarrollo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class ABTipoPropiedadController : Controller
    {
        // GET: ABTipoPropiedad
        [HttpGet]
        public ActionResult CrearTipoPropiedad()
        {
            (ErrorPropy errorPropy, List<DTOTipoAmbiente> dTOTipoAmbientes) resultado = ABMTipoAmbiente.TraerTipoAmbiente();
            (ErrorPropy errorPropy, List<DTOCaracteristica> dTOCaracteristicas) resultado2 = ABMCaracteristica.TraerCaracteristicas();

            try
            {
                if (resultado.errorPropy.codigoError != 0)
                {
                    throw new Exception(resultado.errorPropy.descripcionError);
                }
                else
                {
                    if (resultado2.errorPropy.codigoError != 0)
                    {
                        throw new Exception(resultado2.errorPropy.descripcionError);
                    }
                }
            }
            catch (Exception error)
            {
                ViewBag.Error = error.Message;
                return View("Error");
            }

            DTO_C_Y_A dTO_C_Y_A = new DTO_C_Y_A();
            dTO_C_Y_A.dTOCaracteristicas = resultado2.dTOCaracteristicas;
            dTO_C_Y_A.dTOTipoAmbientes = resultado.dTOTipoAmbientes;

            return View(dTO_C_Y_A);
        }
    }
}