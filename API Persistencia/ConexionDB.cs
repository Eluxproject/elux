﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Persistencia.Models;

namespace API_Persistencia
{
    public class ConexionDB:DbContext
    {
        public ConexionDB()
        {

        }
        public ConexionDB(DbContextOptions<ConexionDB> options)
      : base(options)
        {
        }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Caracteristica> Caracteristica { get; set; }
        public DbSet<Actividad> Actividad { get; set; }
        public DbSet<ImagenPropiedad> ImagenPropiedad { get; set; }
        public DbSet<Plan> Plan { get; set; }
        public DbSet<PlanUsuario> PlanUsuario { get; set; }
        public DbSet<Propiedad> Propiedad { get; set; }
        public DbSet<PropiedadCaracteristica> PropiedadCaracteristica { get; set; }
        public DbSet<PropiedadTipoAmbiente> PropiedadTipoAmbiente { get; set; }
        public DbSet<Publicacion> Publicacion { get; set; }
        public DbSet<TipoAmbiente> TipoAmbiente { get; set; }
        public DbSet<TipoConstruccion> TipoConstruccion { get; set; }
        public DbSet<TipoPropiedad> TipoPropiedad { get; set; }
        public DbSet<TipoPropiedadTipoAmbiente> TipoPropiedadTipoAmbiente { get; set; }
        public DbSet<TipoPublicacion> TipoPublicacion { get; set; }
        public DbSet<TipoPublicante> TipoPublicante { get; set; }
        public DbSet<VisitaInmueble> VisitaInmueble { get; set; }
        public DbSet<VisitaPerfilPublicante> VisitaPerfilPublicante { get; set; }
        public DbSet<TipoMoneda> TipoMoneda { get; set; }
        public DbSet<Credito> Credito { get; set; }
        public DbSet<SolicitudContactoVisitante> SolicitudContactoVisitante { get; set; }
        public DbSet<Favorito> Favorito { get; set; }
        public DbSet<PagoMP> PagoMP { get; set; }
    }
}
