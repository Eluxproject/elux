﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Persistencia.Models
{
    public class Caracteristica
    {
        public string caracteristicaId { get; set; }
        public string nombreCaracteristica { get; set; }
        public bool activo { get; set; }
    }
}
