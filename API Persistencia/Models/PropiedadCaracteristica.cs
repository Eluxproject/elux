﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Persistencia.Models
{
    public class PropiedadCaracteristica
    {
        public string propiedadCaracteristicaId { get; set; }
        public bool activo { get; set; }
        public string propiedadId { get; set; }
        public string caracteristicaId { get; set; }
        public virtual Caracteristica Caracteristica { get; set; }
    }
}
