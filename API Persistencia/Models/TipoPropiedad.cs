﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Persistencia.Models
{
    public class TipoPropiedad
    {
        public string tipoPropiedadId { get; set; }
        public string nombreTipoPropiedad { get; set; }
        public bool activo { get; set; }
       
        public virtual ICollection<TipoPropiedadTipoAmbiente> TipoPropiedadTipoAmbiente { get; set; }
    }
}
