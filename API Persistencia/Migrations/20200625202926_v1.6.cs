﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "tipoMonedaId",
                table: "Propiedad",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tipoMonedaId",
                table: "Plan",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TipoMoneda",
                columns: table => new
                {
                    tipoMonedaId = table.Column<string>(nullable: false),
                    denominacionMoneda = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMoneda", x => x.tipoMonedaId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Propiedad_tipoMonedaId",
                table: "Propiedad",
                column: "tipoMonedaId");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_tipoMonedaId",
                table: "Plan",
                column: "tipoMonedaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Plan_TipoMoneda_tipoMonedaId",
                table: "Plan",
                column: "tipoMonedaId",
                principalTable: "TipoMoneda",
                principalColumn: "tipoMonedaId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Propiedad_TipoMoneda_tipoMonedaId",
                table: "Propiedad",
                column: "tipoMonedaId",
                principalTable: "TipoMoneda",
                principalColumn: "tipoMonedaId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plan_TipoMoneda_tipoMonedaId",
                table: "Plan");

            migrationBuilder.DropForeignKey(
                name: "FK_Propiedad_TipoMoneda_tipoMonedaId",
                table: "Propiedad");

            migrationBuilder.DropTable(
                name: "TipoMoneda");

            migrationBuilder.DropIndex(
                name: "IX_Propiedad_tipoMonedaId",
                table: "Propiedad");

            migrationBuilder.DropIndex(
                name: "IX_Plan_tipoMonedaId",
                table: "Plan");

            migrationBuilder.DropColumn(
                name: "tipoMonedaId",
                table: "Propiedad");

            migrationBuilder.DropColumn(
                name: "tipoMonedaId",
                table: "Plan");
        }
    }
}
