﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v27 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Favorito_Propiedad_propiedadId",
                table: "Favorito");

            migrationBuilder.DropIndex(
                name: "IX_Favorito_propiedadId",
                table: "Favorito");

            migrationBuilder.DropColumn(
                name: "propiedadId",
                table: "Favorito");

            migrationBuilder.AddColumn<string>(
                name: "publicacionId",
                table: "Favorito",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Favorito_publicacionId",
                table: "Favorito",
                column: "publicacionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Favorito_Publicacion_publicacionId",
                table: "Favorito",
                column: "publicacionId",
                principalTable: "Publicacion",
                principalColumn: "publicacionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Favorito_Publicacion_publicacionId",
                table: "Favorito");

            migrationBuilder.DropIndex(
                name: "IX_Favorito_publicacionId",
                table: "Favorito");

            migrationBuilder.DropColumn(
                name: "publicacionId",
                table: "Favorito");

            migrationBuilder.AddColumn<string>(
                name: "propiedadId",
                table: "Favorito",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Favorito_propiedadId",
                table: "Favorito",
                column: "propiedadId");

            migrationBuilder.AddForeignKey(
                name: "FK_Favorito_Propiedad_propiedadId",
                table: "Favorito",
                column: "propiedadId",
                principalTable: "Propiedad",
                principalColumn: "propiedadId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
