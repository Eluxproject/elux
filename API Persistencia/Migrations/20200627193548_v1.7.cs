﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "amueblado",
                table: "Propiedad",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "añosAntiguedad",
                table: "Propiedad",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "importeExpensasUltimoMes",
                table: "Propiedad",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "superficieCubierta",
                table: "Propiedad",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "superficieTerreno",
                table: "Propiedad",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "amueblado",
                table: "Propiedad");

            migrationBuilder.DropColumn(
                name: "añosAntiguedad",
                table: "Propiedad");

            migrationBuilder.DropColumn(
                name: "importeExpensasUltimoMes",
                table: "Propiedad");

            migrationBuilder.DropColumn(
                name: "superficieCubierta",
                table: "Propiedad");

            migrationBuilder.DropColumn(
                name: "superficieTerreno",
                table: "Propiedad");
        }
    }
}
