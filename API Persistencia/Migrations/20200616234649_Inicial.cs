﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    usuarioId = table.Column<string>(nullable: false),
                    nombreUsuario = table.Column<string>(nullable: true),
                    apellidoUsuario = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    clave = table.Column<string>(nullable: true),
                    emailConfirmado = table.Column<bool>(nullable: false),
                    permiterSerContactadoPorPublicante = table.Column<bool>(nullable: false),
                    permiteSerNotificado = table.Column<bool>(nullable: false),
                    telefono1 = table.Column<long>(nullable: false),
                    telefono2 = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.usuarioId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Usuario");
        }
    }
}
