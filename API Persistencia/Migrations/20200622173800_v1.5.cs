﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TipoPublicacion",
                columns: table => new
                {
                    tipoPublicacionId = table.Column<string>(nullable: false),
                    nombreTipoPublicacion = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoPublicacion", x => x.tipoPublicacionId);
                });

            migrationBuilder.CreateTable(
                name: "Publicacion",
                columns: table => new
                {
                    publicacionId = table.Column<string>(nullable: false),
                    fechaInicioPublicacion = table.Column<DateTime>(nullable: false),
                    fechaFinPublicacion = table.Column<DateTime>(nullable: false),
                    activo = table.Column<bool>(nullable: false),
                    tipoPublicacionId = table.Column<string>(nullable: true),
                    propiedadId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publicacion", x => x.publicacionId);
                    table.ForeignKey(
                        name: "FK_Publicacion_Propiedad_propiedadId",
                        column: x => x.propiedadId,
                        principalTable: "Propiedad",
                        principalColumn: "propiedadId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Publicacion_TipoPublicacion_tipoPublicacionId",
                        column: x => x.tipoPublicacionId,
                        principalTable: "TipoPublicacion",
                        principalColumn: "tipoPublicacionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitaInmueble",
                columns: table => new
                {
                    visitaInmuebleId = table.Column<string>(nullable: false),
                    fechaHoraVisitaInmueble = table.Column<DateTime>(nullable: false),
                    usuarioId = table.Column<string>(nullable: true),
                    publicacionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitaInmueble", x => x.visitaInmuebleId);
                    table.ForeignKey(
                        name: "FK_VisitaInmueble_Publicacion_publicacionId",
                        column: x => x.publicacionId,
                        principalTable: "Publicacion",
                        principalColumn: "publicacionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisitaInmueble_Usuario_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VisitaPerfilPublicante",
                columns: table => new
                {
                    visitaPerfilPublicanteId = table.Column<string>(nullable: false),
                    fechaHoraVisitaPerfilPublicante = table.Column<DateTime>(nullable: false),
                    publicacionId = table.Column<string>(nullable: true),
                    usuarioId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitaPerfilPublicante", x => x.visitaPerfilPublicanteId);
                    table.ForeignKey(
                        name: "FK_VisitaPerfilPublicante_Publicacion_publicacionId",
                        column: x => x.publicacionId,
                        principalTable: "Publicacion",
                        principalColumn: "publicacionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisitaPerfilPublicante_Usuario_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Publicacion_propiedadId",
                table: "Publicacion",
                column: "propiedadId");

            migrationBuilder.CreateIndex(
                name: "IX_Publicacion_tipoPublicacionId",
                table: "Publicacion",
                column: "tipoPublicacionId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitaInmueble_publicacionId",
                table: "VisitaInmueble",
                column: "publicacionId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitaInmueble_usuarioId",
                table: "VisitaInmueble",
                column: "usuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitaPerfilPublicante_publicacionId",
                table: "VisitaPerfilPublicante",
                column: "publicacionId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitaPerfilPublicante_usuarioId",
                table: "VisitaPerfilPublicante",
                column: "usuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VisitaInmueble");

            migrationBuilder.DropTable(
                name: "VisitaPerfilPublicante");

            migrationBuilder.DropTable(
                name: "Publicacion");

            migrationBuilder.DropTable(
                name: "TipoPublicacion");
        }
    }
}
