﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v40 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "NumFactura",
                table: "PlanUsuario",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "PagoMPId",
                table: "PlanUsuario",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PagoMP",
                columns: table => new
                {
                    PagoMPId = table.Column<string>(nullable: false),
                    idPago = table.Column<string>(nullable: true),
                    currency_id = table.Column<string>(nullable: true),
                    date_approved = table.Column<DateTime>(nullable: false),
                    date_created = table.Column<DateTime>(nullable: false),
                    operation_type = table.Column<string>(nullable: true),
                    payment_type_id = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    transaction_amount = table.Column<float>(nullable: false),
                    UsuarioId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PagoMP", x => x.PagoMPId);
                    table.ForeignKey(
                        name: "FK_PagoMP_Usuario_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlanUsuario_PagoMPId",
                table: "PlanUsuario",
                column: "PagoMPId");

            migrationBuilder.CreateIndex(
                name: "IX_PagoMP_UsuarioId",
                table: "PagoMP",
                column: "UsuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanUsuario_PagoMP_PagoMPId",
                table: "PlanUsuario",
                column: "PagoMPId",
                principalTable: "PagoMP",
                principalColumn: "PagoMPId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanUsuario_PagoMP_PagoMPId",
                table: "PlanUsuario");

            migrationBuilder.DropTable(
                name: "PagoMP");

            migrationBuilder.DropIndex(
                name: "IX_PlanUsuario_PagoMPId",
                table: "PlanUsuario");

            migrationBuilder.DropColumn(
                name: "NumFactura",
                table: "PlanUsuario");

            migrationBuilder.DropColumn(
                name: "PagoMPId",
                table: "PlanUsuario");
        }
    }
}
