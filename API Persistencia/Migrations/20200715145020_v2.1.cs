﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaAdmNivel3",
                table: "Propiedad");

            migrationBuilder.AddColumn<string>(
                name: "pais",
                table: "Propiedad",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "pais",
                table: "Propiedad");

            migrationBuilder.AddColumn<string>(
                name: "AreaAdmNivel3",
                table: "Propiedad",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
