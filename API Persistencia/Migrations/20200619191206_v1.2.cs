﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Caracteristica",
                columns: table => new
                {
                    caracteristicaId = table.Column<string>(nullable: false),
                    nombreCaracteristica = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Caracteristica", x => x.caracteristicaId);
                });

            migrationBuilder.CreateTable(
                name: "Plan",
                columns: table => new
                {
                    planId = table.Column<string>(nullable: false),
                    nombrePlan = table.Column<string>(nullable: true),
                    permiteVideo = table.Column<bool>(nullable: false),
                    accesoEstadisticasAvanzadas = table.Column<bool>(nullable: false),
                    cantidadCreditosIniciales = table.Column<int>(nullable: false),
                    precioPlan = table.Column<decimal>(nullable: false),
                    cantidadMaxImagenesPermitidasPorPublicacion = table.Column<int>(nullable: false),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan", x => x.planId);
                });

            migrationBuilder.CreateTable(
                name: "TipoAmbiente",
                columns: table => new
                {
                    tipoAmbienteId = table.Column<string>(nullable: false),
                    nombreTipoAmbiente = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoAmbiente", x => x.tipoAmbienteId);
                });

            migrationBuilder.CreateTable(
                name: "TipoConstruccion",
                columns: table => new
                {
                    tipoConstruccionId = table.Column<string>(nullable: false),
                    nombreTipoConstruccion = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoConstruccion", x => x.tipoConstruccionId);
                });

            migrationBuilder.CreateTable(
                name: "TipoPropiedad",
                columns: table => new
                {
                    tipoPropiedadId = table.Column<string>(nullable: false),
                    nombreTipoPropiedad = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoPropiedad", x => x.tipoPropiedadId);
                });

            migrationBuilder.CreateTable(
                name: "TipoPublicante",
                columns: table => new
                {
                    tipoPublicanteId = table.Column<string>(nullable: false),
                    nombreTipoPublicante = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoPublicante", x => x.tipoPublicanteId);
                });

            migrationBuilder.CreateTable(
                name: "PlanUsuario",
                columns: table => new
                {
                    planUsuarioId = table.Column<string>(nullable: false),
                    activo = table.Column<bool>(nullable: false),
                    fechaContratacion = table.Column<DateTime>(nullable: false),
                    cantidadCreditosActivos = table.Column<int>(nullable: false),
                    planId = table.Column<string>(nullable: true),
                    usuarioId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanUsuario", x => x.planUsuarioId);
                    table.ForeignKey(
                        name: "FK_PlanUsuario_Plan_planId",
                        column: x => x.planId,
                        principalTable: "Plan",
                        principalColumn: "planId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlanUsuario_Usuario_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TipoPropiedadTipoAmbiente",
                columns: table => new
                {
                    tipoPropiedadTipoAmbienteId = table.Column<string>(nullable: false),
                    activo = table.Column<bool>(nullable: false),
                    tipoAmbienteId = table.Column<string>(nullable: true),
                    tipoPropiedadId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoPropiedadTipoAmbiente", x => x.tipoPropiedadTipoAmbienteId);
                    table.ForeignKey(
                        name: "FK_TipoPropiedadTipoAmbiente_TipoAmbiente_tipoAmbienteId",
                        column: x => x.tipoAmbienteId,
                        principalTable: "TipoAmbiente",
                        principalColumn: "tipoAmbienteId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TipoPropiedadTipoAmbiente_TipoPropiedad_tipoPropiedadId",
                        column: x => x.tipoPropiedadId,
                        principalTable: "TipoPropiedad",
                        principalColumn: "tipoPropiedadId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Propiedad",
                columns: table => new
                {
                    propiedadId = table.Column<string>(nullable: false),
                    ubicacion = table.Column<string>(nullable: true),
                    AreaAdmNivel1 = table.Column<string>(nullable: true),
                    AreaAdmNivel2 = table.Column<string>(nullable: true),
                    AreaAdmNivel3 = table.Column<string>(nullable: true),
                    latitud = table.Column<double>(nullable: false),
                    longitud = table.Column<double>(nullable: false),
                    precioPropiedad = table.Column<decimal>(nullable: false),
                    fechaRegistro = table.Column<DateTime>(nullable: false),
                    tipoPublicanteId = table.Column<string>(nullable: true),
                    tipoConstruccionId = table.Column<string>(nullable: true),
                    tipoPropiedadId = table.Column<string>(nullable: true),
                    usuarioId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Propiedad", x => x.propiedadId);
                    table.ForeignKey(
                        name: "FK_Propiedad_TipoConstruccion_tipoConstruccionId",
                        column: x => x.tipoConstruccionId,
                        principalTable: "TipoConstruccion",
                        principalColumn: "tipoConstruccionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Propiedad_TipoPropiedad_tipoPropiedadId",
                        column: x => x.tipoPropiedadId,
                        principalTable: "TipoPropiedad",
                        principalColumn: "tipoPropiedadId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Propiedad_TipoPublicante_tipoPublicanteId",
                        column: x => x.tipoPublicanteId,
                        principalTable: "TipoPublicante",
                        principalColumn: "tipoPublicanteId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Propiedad_Usuario_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Favorito",
                columns: table => new
                {
                    favoritoId = table.Column<string>(nullable: false),
                    activo = table.Column<bool>(nullable: false),
                    propiedadId = table.Column<string>(nullable: true),
                    usuarioId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Favorito", x => x.favoritoId);
                    table.ForeignKey(
                        name: "FK_Favorito_Propiedad_propiedadId",
                        column: x => x.propiedadId,
                        principalTable: "Propiedad",
                        principalColumn: "propiedadId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Favorito_Usuario_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ImagenPropiedad",
                columns: table => new
                {
                    ImagenPropiedadId = table.Column<string>(nullable: false),
                    rutaImagenPropiedad = table.Column<string>(nullable: true),
                    activo = table.Column<bool>(nullable: false),
                    propiedadId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImagenPropiedad", x => x.ImagenPropiedadId);
                    table.ForeignKey(
                        name: "FK_ImagenPropiedad_Propiedad_propiedadId",
                        column: x => x.propiedadId,
                        principalTable: "Propiedad",
                        principalColumn: "propiedadId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropiedadCaracteristica",
                columns: table => new
                {
                    propiedadCaracteristicaId = table.Column<string>(nullable: false),
                    activo = table.Column<bool>(nullable: false),
                    propiedadId = table.Column<string>(nullable: true),
                    caracteristicaId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropiedadCaracteristica", x => x.propiedadCaracteristicaId);
                    table.ForeignKey(
                        name: "FK_PropiedadCaracteristica_Caracteristica_caracteristicaId",
                        column: x => x.caracteristicaId,
                        principalTable: "Caracteristica",
                        principalColumn: "caracteristicaId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropiedadCaracteristica_Propiedad_propiedadId",
                        column: x => x.propiedadId,
                        principalTable: "Propiedad",
                        principalColumn: "propiedadId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropiedadTipoAmbiente",
                columns: table => new
                {
                    propiedadTipoAmbienteId = table.Column<string>(nullable: false),
                    activo = table.Column<bool>(nullable: false),
                    cantidad = table.Column<int>(nullable: false),
                    propiedadId = table.Column<string>(nullable: true),
                    tipoAmbienteId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropiedadTipoAmbiente", x => x.propiedadTipoAmbienteId);
                    table.ForeignKey(
                        name: "FK_PropiedadTipoAmbiente_Propiedad_propiedadId",
                        column: x => x.propiedadId,
                        principalTable: "Propiedad",
                        principalColumn: "propiedadId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PropiedadTipoAmbiente_TipoAmbiente_tipoAmbienteId",
                        column: x => x.tipoAmbienteId,
                        principalTable: "TipoAmbiente",
                        principalColumn: "tipoAmbienteId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Favorito_propiedadId",
                table: "Favorito",
                column: "propiedadId");

            migrationBuilder.CreateIndex(
                name: "IX_Favorito_usuarioId",
                table: "Favorito",
                column: "usuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_ImagenPropiedad_propiedadId",
                table: "ImagenPropiedad",
                column: "propiedadId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanUsuario_planId",
                table: "PlanUsuario",
                column: "planId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanUsuario_usuarioId",
                table: "PlanUsuario",
                column: "usuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Propiedad_tipoConstruccionId",
                table: "Propiedad",
                column: "tipoConstruccionId");

            migrationBuilder.CreateIndex(
                name: "IX_Propiedad_tipoPropiedadId",
                table: "Propiedad",
                column: "tipoPropiedadId");

            migrationBuilder.CreateIndex(
                name: "IX_Propiedad_tipoPublicanteId",
                table: "Propiedad",
                column: "tipoPublicanteId");

            migrationBuilder.CreateIndex(
                name: "IX_Propiedad_usuarioId",
                table: "Propiedad",
                column: "usuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_PropiedadCaracteristica_caracteristicaId",
                table: "PropiedadCaracteristica",
                column: "caracteristicaId");

            migrationBuilder.CreateIndex(
                name: "IX_PropiedadCaracteristica_propiedadId",
                table: "PropiedadCaracteristica",
                column: "propiedadId");

            migrationBuilder.CreateIndex(
                name: "IX_PropiedadTipoAmbiente_propiedadId",
                table: "PropiedadTipoAmbiente",
                column: "propiedadId");

            migrationBuilder.CreateIndex(
                name: "IX_PropiedadTipoAmbiente_tipoAmbienteId",
                table: "PropiedadTipoAmbiente",
                column: "tipoAmbienteId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoPropiedadTipoAmbiente_tipoAmbienteId",
                table: "TipoPropiedadTipoAmbiente",
                column: "tipoAmbienteId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoPropiedadTipoAmbiente_tipoPropiedadId",
                table: "TipoPropiedadTipoAmbiente",
                column: "tipoPropiedadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Favorito");

            migrationBuilder.DropTable(
                name: "ImagenPropiedad");

            migrationBuilder.DropTable(
                name: "PlanUsuario");

            migrationBuilder.DropTable(
                name: "PropiedadCaracteristica");

            migrationBuilder.DropTable(
                name: "PropiedadTipoAmbiente");

            migrationBuilder.DropTable(
                name: "TipoPropiedadTipoAmbiente");

            migrationBuilder.DropTable(
                name: "Plan");

            migrationBuilder.DropTable(
                name: "Caracteristica");

            migrationBuilder.DropTable(
                name: "Propiedad");

            migrationBuilder.DropTable(
                name: "TipoAmbiente");

            migrationBuilder.DropTable(
                name: "TipoConstruccion");

            migrationBuilder.DropTable(
                name: "TipoPropiedad");

            migrationBuilder.DropTable(
                name: "TipoPublicante");
        }
    }
}
