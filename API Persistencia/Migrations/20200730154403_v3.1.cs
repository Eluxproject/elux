﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v31 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Activo",
                table: "Credito",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NombrePack",
                table: "Credito",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tipoMonedaId",
                table: "Credito",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Credito_tipoMonedaId",
                table: "Credito",
                column: "tipoMonedaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Credito_TipoMoneda_tipoMonedaId",
                table: "Credito",
                column: "tipoMonedaId",
                principalTable: "TipoMoneda",
                principalColumn: "tipoMonedaId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Credito_TipoMoneda_tipoMonedaId",
                table: "Credito");

            migrationBuilder.DropIndex(
                name: "IX_Credito_tipoMonedaId",
                table: "Credito");

            migrationBuilder.DropColumn(
                name: "Activo",
                table: "Credito");

            migrationBuilder.DropColumn(
                name: "NombrePack",
                table: "Credito");

            migrationBuilder.DropColumn(
                name: "tipoMonedaId",
                table: "Credito");
        }
    }
}
