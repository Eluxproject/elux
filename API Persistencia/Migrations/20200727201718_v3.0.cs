﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v30 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreditoPaqueteID",
                table: "PlanUsuario",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaCompra",
                table: "PlanUsuario",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "Credito",
                columns: table => new
                {
                    PaqueteID = table.Column<string>(nullable: false),
                    CantidadCreditos = table.Column<int>(nullable: false),
                    Precio = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Credito", x => x.PaqueteID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlanUsuario_CreditoPaqueteID",
                table: "PlanUsuario",
                column: "CreditoPaqueteID");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanUsuario_Credito_CreditoPaqueteID",
                table: "PlanUsuario",
                column: "CreditoPaqueteID",
                principalTable: "Credito",
                principalColumn: "PaqueteID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanUsuario_Credito_CreditoPaqueteID",
                table: "PlanUsuario");

            migrationBuilder.DropTable(
                name: "Credito");

            migrationBuilder.DropIndex(
                name: "IX_PlanUsuario_CreditoPaqueteID",
                table: "PlanUsuario");

            migrationBuilder.DropColumn(
                name: "CreditoPaqueteID",
                table: "PlanUsuario");

            migrationBuilder.DropColumn(
                name: "FechaCompra",
                table: "PlanUsuario");
        }
    }
}
