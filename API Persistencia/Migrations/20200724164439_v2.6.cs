﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v26 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SolicitudContactoVisitante",
                columns: table => new
                {
                    solicitudContactoVisitanteId = table.Column<string>(nullable: false),
                    cantidadVecesRealizoSolicitud = table.Column<int>(nullable: false),
                    fechaSolicitud = table.Column<DateTime>(nullable: false),
                    publicacionId = table.Column<string>(nullable: true),
                    usuarioId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SolicitudContactoVisitante", x => x.solicitudContactoVisitanteId);
                    table.ForeignKey(
                        name: "FK_SolicitudContactoVisitante_Publicacion_publicacionId",
                        column: x => x.publicacionId,
                        principalTable: "Publicacion",
                        principalColumn: "publicacionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SolicitudContactoVisitante_Usuario_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuario",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SolicitudContactoVisitante_publicacionId",
                table: "SolicitudContactoVisitante",
                column: "publicacionId");

            migrationBuilder.CreateIndex(
                name: "IX_SolicitudContactoVisitante_usuarioId",
                table: "SolicitudContactoVisitante",
                column: "usuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SolicitudContactoVisitante");
        }
    }
}
