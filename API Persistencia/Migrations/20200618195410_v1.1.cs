﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "clave",
                table: "Usuario");

            migrationBuilder.AddColumn<byte[]>(
                name: "Key",
                table: "Usuario",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Vector",
                table: "Usuario",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "contraseña",
                table: "Usuario",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Key",
                table: "Usuario");

            migrationBuilder.DropColumn(
                name: "Vector",
                table: "Usuario");

            migrationBuilder.DropColumn(
                name: "contraseña",
                table: "Usuario");

            migrationBuilder.AddColumn<string>(
                name: "clave",
                table: "Usuario",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
