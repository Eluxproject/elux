﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v33 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanUsuario_Credito_CreditoPaqueteID",
                table: "PlanUsuario");

            migrationBuilder.RenameColumn(
                name: "CreditoPaqueteID",
                table: "PlanUsuario",
                newName: "CreditoPaqueteId");

            migrationBuilder.RenameIndex(
                name: "IX_PlanUsuario_CreditoPaqueteID",
                table: "PlanUsuario",
                newName: "IX_PlanUsuario_CreditoPaqueteId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanUsuario_Credito_CreditoPaqueteId",
                table: "PlanUsuario",
                column: "CreditoPaqueteId",
                principalTable: "Credito",
                principalColumn: "PaqueteID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlanUsuario_Credito_CreditoPaqueteId",
                table: "PlanUsuario");

            migrationBuilder.RenameColumn(
                name: "CreditoPaqueteId",
                table: "PlanUsuario",
                newName: "CreditoPaqueteID");

            migrationBuilder.RenameIndex(
                name: "IX_PlanUsuario_CreditoPaqueteId",
                table: "PlanUsuario",
                newName: "IX_PlanUsuario_CreditoPaqueteID");

            migrationBuilder.AddForeignKey(
                name: "FK_PlanUsuario_Credito_CreditoPaqueteID",
                table: "PlanUsuario",
                column: "CreditoPaqueteID",
                principalTable: "Credito",
                principalColumn: "PaqueteID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
