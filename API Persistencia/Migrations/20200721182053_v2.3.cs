﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "obtuvoContactoPublicante",
                table: "VisitaInmueble");

            migrationBuilder.AddColumn<bool>(
                name: "contactoPublicante",
                table: "VisitaInmueble",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "contactoPublicante",
                table: "VisitaInmueble");

            migrationBuilder.AddColumn<bool>(
                name: "obtuvoContactoPublicante",
                table: "VisitaInmueble",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
