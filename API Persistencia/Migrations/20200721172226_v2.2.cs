﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Persistencia.Migrations
{
    public partial class v22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "cantidadVecesQueRepitioVisita",
                table: "VisitaInmueble",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "obtuvoContactoPublicante",
                table: "VisitaInmueble",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "cantidadVecesQueRepitioVisita",
                table: "VisitaInmueble");

            migrationBuilder.DropColumn(
                name: "obtuvoContactoPublicante",
                table: "VisitaInmueble");
        }
    }
}
