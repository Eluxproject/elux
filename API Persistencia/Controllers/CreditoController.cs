﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using API_Persistencia.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Persistencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditoController : ControllerBase
    {
        private ConexionDB con;
        public CreditoController(ConexionDB conexion)
        {
            con = conexion;
        }
        [HttpGet("Obtener_Packs_Creditos")]
        public List<Credito> Obtener_Packs_Creditos()
        {
            List<Credito> creditos = (from x in con.Credito.Include(x => x.TipoMoneda) where x.Activo == true orderby x.Precio ascending select x).ToList();

            return creditos;
        }

        [HttpGet("ObtenerPorID/{id}")]
        public Credito ObtenerPorID(string id)
        {
            Credito credito = (from x in con.Credito.Include(x => x.TipoMoneda) where x.PaqueteID == id select x).FirstOrDefault();

            return credito;
        }
    }
}
