﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Persistencia.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Persistencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoMonedaController : ControllerBase
    {
        private ConexionDB con;
        public TipoMonedaController(ConexionDB conexion)
        {
            con = conexion;
        }
        [HttpGet("obtenerTiposMonedas")]
        public List<TipoMoneda> ObtenerTodos() {
            List<TipoMoneda> tiposMonedas = (from x in con.TipoMoneda
                                             where x.activo == true
                                             select x).ToList();
            return tiposMonedas;
        }
    }
}