﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Persistencia.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Persistencia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusquedaController : ControllerBase
    {
        private ConexionDB con;
        public BusquedaController(ConexionDB conexion)
        {
            con = conexion;
        }
        [HttpGet("obtenerPropiedadesParaEvaluarBusqueda")]
        public List<Publicacion> ObtenerPropiedadesParaEvaluarBusqueda() {
            List<Publicacion> publicaciones = (from x in con.Publicacion
                                             .Include(x=>x.Propiedad)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.ImagenPropiedad)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.PropiedadCaracteristica).ThenInclude(x=>x.Caracteristica)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.PropiedadTipoAmbiente).ThenInclude(x=>x.TipoAmbiente)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.TipoPropiedad)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.TipoConstruccion)
                                             .Include(x=>x.TipoPublicacion)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.TipoPublicante)
                                             .Include(x=>x.Propiedad).ThenInclude(x=>x.TipoMoneda)
                                           where x.estado == 0
                                           select x).ToList();

            return publicaciones;
        }

    }
}
